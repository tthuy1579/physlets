import { createStore } from 'redux';
import { fromJS } from 'immutable';

import createReducer from './reducers';

export default function configureStore(initialState = {}) {
  return createStore(
    createReducer(),
    fromJS(initialState),
  );
}
