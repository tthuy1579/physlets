import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'sanitize.css/sanitize.css';

import HomePage from 'containers/HomePage';

import '!file-loader?name=[name].[ext]!./favicon.png';
import '!file-loader?name=[name].[ext]!./manifest.json';

import configureStore from './store';
import './global-styles';

window.API = process.env.API || 'http://localhost:8081';

const store = configureStore();

export function getStore() {
  return store;
}

ReactDOM.render(
  <Provider store={store}>
    <HomePage />
  </Provider>,
  document.getElementById('app')
);
